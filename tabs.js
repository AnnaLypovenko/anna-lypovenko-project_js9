
const tabsCaptionLi = document.querySelectorAll('.tabs li');

for (let i = 0; i < tabsCaptionLi.length; i++) {
    tabsCaptionLi[i].addEventListener('click', switchTab);
}

function switchTab() {
    const elemUl = this.parentElement;
    const allListItems = elemUl.children;
    const arrayListItems = Array.prototype.slice.call(allListItems);

    arrayListItems.forEach(function(item) {
        item.classList.remove('active');
    });
    this.classList.add('active');
    const tabsContent = document.querySelectorAll('.centered-content .tabs__content');
    for (let i = 0; i < tabsContent.length; i++) {
        tabsContent[i].classList.remove('active');
    }

    const position = arrayListItems.indexOf(this);

    tabsContent[position].classList.add('active');
}
